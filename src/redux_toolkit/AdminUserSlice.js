import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  userList: [],
};
const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setUserList: (state, action) => {
      state.userList = action.payload;
    },
    setAddUser: (state, action) => {
      let newUserList = [...state.userList];
      let cloneUsers = {...action.payload}
      newUserList.push(cloneUsers);
      state.userList = newUserList;
    },
    setDeleteUser: (state, action) => {
      let newUerList = [...state.userList];
      let index = newUerList.findIndex((item) => {
        return item.taiKhoan === action.payload;
      });
      if (index >= 0) {
        newUerList.slice(index, 1);
        state.userList = newUerList;
      }
    },
    setUserUpdate: (state, action) => {
      let newUserList = [...state.userList];
      console.log(action.payload);
      let index = newUserList.findIndex((item) => {
        return item.hoTen === action.payload;
      });
      if (index == -1) {
        return;
      }

      let cloneUpdateUsers = { ...action.payload };
      newUserList = cloneUpdateUsers;
      state.userList = newUserList;
    },
  },
});

export const { setUserList, setDeleteUser, setAddUser, setUserUpdate } =
  userSlice.actions;
export default userSlice.reducer;
