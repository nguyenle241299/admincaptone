import { createSlice } from "@reduxjs/toolkit";
import { userLocalStorage } from "../service/adminLocalService";

const initialState = {
  admin: userLocalStorage.get(),
};

const adminSlice = createSlice({
  name: "adminSlice",
  initialState,
  reducers: {
    setAdminInfo: (state, action) => {
      state.admin = action.payload;
    },
  },
});

export const { setAdminInfo } = adminSlice.actions;
export default adminSlice.reducer;
