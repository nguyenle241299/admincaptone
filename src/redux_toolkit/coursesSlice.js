import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  coursesList: [],
};

const coursesSlice = createSlice({
  name: "coursesSlice",
  initialState,
  reducers: {
    setCoursesList: (state, action) => {
      state.coursesList = action.payload;
    },
    setDeleteCourses: (state, action) => {
      let newCoursesList = [...state.coursesList];
      const index = newCoursesList.findIndex((item) => {
        return item.maKhoaHoc === action.payload;
      });
      console.log(index);
      if (index !== -1) {
        newCoursesList.splice(index, 1);
        state.coursesList = newCoursesList;
      }
    },
    setAddCourses: (state, action) => {
      console.log(action.payload);
      let newCoursesList = [...state.coursesList];
      let index = newCoursesList.findIndex((item) => {
        return item.maKhoaHoc === action.payload;
      });
      if (index == -1) {
        let cloneCourses = { ...action.payload };
        newCoursesList.push(cloneCourses);
        state.coursesList = newCoursesList;
      }
    },
    setUpdateCourses: (state, action) => {
      let newCourseList = [...state.coursesList];
      let index = newCourseList.findIndex((item) => {
        return item.maKhoaHoc === action.payload.maKhoaHoc;
      });
      if (index == -1) {
        return;
      }
      let cloneUpdateCourses = { ...action.payload };
      newCourseList[index] = cloneUpdateCourses;
      state.coursesList = newCourseList;
    },
  },
});

export const {
  setCoursesList,
  setDeleteCourses,
  setAddCourses,
  setUpdateCourses,
} = coursesSlice.actions;

export default coursesSlice.reducer;
