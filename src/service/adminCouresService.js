import { base_URL } from "./configURL";

export const adminCoursesService = {
  getCoursesList: () => {
    return base_URL.get("/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?MaNhom=GP01");
  },
  postCourses: (courses) => {
    return base_URL.post("/api/QuanLyKhoaHoc/ThemKhoaHoc", courses);
  },
  deleteCourses: (coursesId) => {
    return base_URL.delete(
      `/api/QuanLyKhoaHoc/XoaKhoaHoc?MaKhoaHoc=${coursesId}`
    );
  },
  putUpdateCourses: (courses) => {
    return base_URL.put("/api/QuanLyKhoaHoc/CapNhatKhoaHoc", courses);
  },
};
