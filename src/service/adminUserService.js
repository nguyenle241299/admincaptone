import { base_URL } from "./configURL";

export const adminUserService = {
  getUserList: () => {
    return base_URL.get("/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP05");
  },
  deleteUser: (account) => {
    return base_URL.delete(
      `/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${account}`
    );
  },
  addUser: (formData) => {
    return base_URL.post("/api/QuanLyNguoiDung/ThemNguoiDung",formData);
  },
  updateUser: (data) => {
    return base_URL.put(`/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung`, data)
  }
};
