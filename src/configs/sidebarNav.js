const sidebarNav = [
  {
    link: "/home",
    section: "home",
    icon: <i className="bx bx-home-alt"></i>,
    text: "Home",
  },
  {
    link: "/users",
    section: "users",
    icon: <i className="bx bx-receipt"></i>,
    text: "Users",
  },
  {
    link: "/management/courses",
    section: "management/courses",
    icon: <i className="bx bx-cube"></i>,
    text: "Courses",
  },
  {
    link: "/addnews",
    section: "customers",
    icon: <i className="bx bx-user"></i>,
    text: "ADD NEW USERS",
  },
  {
    link: "/management/courses",
    section: "stats",
    icon: <i className="bx bx-line-chart"></i>,
    text: "ADD NEW COURSE",
  },
  {
    link: "/login",
    section: "login",
    icon: <i class="bx bx-log-in-circle"></i>,
    text: "Login",
  },
];

export default sidebarNav;
