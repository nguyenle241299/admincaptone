import { configureStore } from "@reduxjs/toolkit";
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import App from "./App";
import adminSlice from "./redux_toolkit/adminSlice";
import coursesSlice from "./redux_toolkit/coursesSlice";
import spinnerSlice from "./redux_toolkit/spinnerSlice";
import userSlice from "./redux_toolkit/AdminUserSlice"



export const store = configureStore({
  reducer: {
    adminSlice,
    coursesSlice,
    spinnerSlice,
    userSlice,
  },
});

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
