import "./assets/libs/boxicons-2.1.1/css/boxicons.min.css";
import "./scss/App.scss";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainLayout from "./layout/MainLayout";
import LoginPage from "./pages/LoginPage/LoginPage";
import HomePage from "./pages/HomePage/HomePage";
import CoursesManagement from "./pages/CoursesManagement/CoursesManagement";
import UserList from "./components/UserList/UserList";
import AddNewusers from "./pages/Add new/AddNewusers";
import Spinner from "./components/Spinner/Spinner";
import EditPage from "./pages/Editeds/EditPage";
import NotFoundPage from "./pages/NotFoundPage/NotFoundPage";

function App() {
  return (
    <div>
      <Spinner />
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<MainLayout />}>
            <Route path="/management/courses" element={<CoursesManagement />} />
            <Route path="login" element={<LoginPage />} />
            <Route path="/home" element={<HomePage />} />
            <Route path="users" element={<UserList />} />
            <Route path="/edit" element={<EditPage />} />
            <Route path="/addnews" element={<AddNewusers />} />
            <Route path="*" element={<NotFoundPage />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
