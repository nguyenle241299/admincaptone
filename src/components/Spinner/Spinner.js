import React from "react";
import { useSelector } from "react-redux";
import { PropagateLoader } from "react-spinners";

export default function Spinner() {
  const isLoading = useSelector((state) => {
    return state.spinnerSlice.isLoading;
  });
  return isLoading ? (
    <div className="fixed bg-slate-100/[.02] h-screen w-screen top-0 left-0 z-[9999] flex items-center justify-center">
      <PropagateLoader size={10} speedMultiplier={1} color="#e33837" />
    </div>
  ) : (
    <></>
  );
}
