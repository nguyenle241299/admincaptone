import React, { useState, useRef } from "react";
import { useEffect } from "react";
import { adminUserService } from "./../../service/adminUserService";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { setDeleteUser, setUserList } from "../../redux_toolkit/AdminUserSlice";
import { message } from "antd";
import { NavLink, useNavigate } from "react-router-dom";

export default function UserList() {
  const userList = useSelector((state) => {
    return state.userSlice.userList;
  });

  const handeReload = () => {
    setTimeout(() => {
      window.location.reload();
    }, 500);
  };

  const [listUserTemp, setListUserTemp] = useState();
  const [search, setSearch] = useState("");
  const typingTimeOutRef = useRef();
  const navigate = useNavigate();

  const dispatch = useDispatch();
  useEffect(() => {
    adminUserService
      .getUserList()
      .then((res) => {
        dispatch(setUserList(res?.data));
        setListUserTemp(res?.data);
      })
      .catch((err) => {
        handeReload()
      });
  }, []);

  const handleRemoveUser = (account) => {
    adminUserService
      .deleteUser(account)
      .then((res) => {
        dispatch(setDeleteUser(account));
        message.success("Delete success");
        handeReload()
      })
      .catch((err) => {
        message.error("Delete failed");
        handeReload()
      });
  };

  const handleRenderUserList = () => {
    return userList.map((item, index) => {
      return (
        <tr
          className="bg-white border-b dark:bg-gray-900 dark:border-gray-700"
          key={index}
        >
          <th
            scope="row"
            className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
          >
            {item.hoTen}
          </th>
          <td className="px-5 py-3">{item.taiKhoan}</td>
          <td className="px-5 py-3">{item.email}</td>
          <td className="px-5 py-3 text-blue-400">
            {item.maLoaiNguoiDung === "GV" ? (<p className="text-red-400">Lecturers</p>) : "Student"}
          </td>
          <td>
            <button
              onClick={() => {
                handleRemoveUser(item.taiKhoan);
              }}
              className="mr-2 px-3 rounded-md py-1 bg-red-600 text-white hover:opacity-75 transition-all"
            >
              Delete
            </button>
            <NavLink
              to="/edit"
              className="px-3 mt-2 rounded-md py-1 bg-sky-600 text-white hover:opacity-75 transition-all"
            >
              Edit
            </NavLink>
          </td>
        </tr>
      );
    });
  };

  const handleChange = (e) => {
    const values = e.target.value;
    setSearch(values);
    if (typingTimeOutRef.current) {
      clearTimeout(typingTimeOutRef.current);
    }
    typingTimeOutRef.current = setTimeout(() => {
      if (values != "") {
        const filterUser = userList.filter((item) => {
          return item.hoTen.toLowerCase().includes(search.toLowerCase());
        });
        dispatch(setUserList(filterUser));
      } else {
        dispatch(setUserList(listUserTemp));
      }
    });
  };

  return (
    <div>
      <div className="mb-6 space-x-10">
        <button
          onClick={() => {
            navigate("/addnews");
          }}
          className="text-white hover:bg-green-400 transition-all bg-green-500 rounded-md py-2 px-6 shadow-md"
        >
          Add Users
        </button>
        <input
          onChange={handleChange}
          type="text"
          value={search}
          className="text-md py-2 px-4 outline-none rounded-md shadow-md"
          placeholder="Search..."
        />
      </div>
      <table className="text-sm text-left text-gray-500 dark:text-gray-400 table w-full">
        <thead className="text-sm text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
          <th scope="col" className="px-6 py-2">
            Full Name
          </th>
          <th scope="col" className="px-4 py-2">
            Username
          </th>
          <th scope="col" className="px-6 py-2">
            Email
          </th>
          <th scope="col" className="px-6 py-2">
            User Type
          </th>
          <th scope="col" className="px-6 py-2">
            Action
          </th>
        </thead>
        <tbody>{handleRenderUserList()}</tbody>
      </table>
    </div>
  );
}
