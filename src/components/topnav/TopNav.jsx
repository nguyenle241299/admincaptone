import React from 'react'
import './topnav.scss'
import UserInfo from '../user-info/UserInfo'
import { data } from '../../constants'
import { useSelector } from 'react-redux';

const TopNav = () => {
    const openSidebar = () => {
        document.body.classList.add('sidebar-open')
    }
    const adminData = useSelector((state) => {
        return state.adminSlice.admin
    })
    console.log('adminData: ', adminData);

    return (
        <div className='topnav'>
            {adminData && <UserInfo user={data.user} />}
            <div className="sidebar-toggle" onClick={openSidebar}>
                <i className='bx bx-menu-alt-right'></i>
            </div>
        </div>
    )
}

export default TopNav
