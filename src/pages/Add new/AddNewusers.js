import React from "react";
import { Button, Form, Input, message, Select } from "antd";
// import { useDispatch } from 'react-redux';
import { useState } from "react";
import { adminUserService } from "./../../service/adminUserService";
import { useDispatch } from "react-redux";
import { setAddUser } from "../../redux_toolkit/AdminUserSlice";
import { Option } from "antd/es/mentions";
import { useNavigate } from "react-router-dom";

export default function AddNewusers() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [formValue, setFormValue] = useState([
    {
      hoTen: "",
      email: "",
      maLoaiNguoiDung: "",
      matKhau: "",
      soDt: "",
      maNhom: "GP05",
      taiKhoan: "",
    },
  ]);

  const handleChange = (e) => {
    let { name, value } = e.target;
    setFormValue({ ...formValue, [name]: value });
  };

  const handleSubmit = (e) => {
    const allInput = {
      hoTen: formValue.hoTen,
      email: formValue.email,
      maLoaiNguoiDung: formValue.maLoaiNguoiDung,
      matKhau: formValue.matKhau,
      soDt: formValue.soDt,
      maNhom: "GP05",
      taiKhoan: formValue.taiKhoan,
    };
    adminUserService
      .addUser(allInput)
      .then((res) => {
        message.success("Succesful");
        dispatch(setAddUser(allInput));
        setTimeout(() => {
          navigate("/users");
        }, 1000);
      })
      .catch((err) => {
        message.error("Failed");
        setTimeout(() => {
          window.location.reload();
        }, 500);
      });
  };

  // };
  return (
    <div>
      <Form>
        <Form.Item
          style={{ width: 500 }}
          rules={[{ required: true, message: "This field is required " }]}
          label="username"
          name="taiKhoan"
        >
          <Input
            onChange={handleChange}
            name="taiKhoan"
            value={formValue.taiKhoan}
            placeholder="input placeholder"
          />
        </Form.Item>
        <Form.Item
          style={{ width: 500 }}
          rules={[{ required: true, message: "This field is required" }]}
          label="password"
          name="matKhau"
        >
          <Input
            onChange={handleChange}
            name="matKhau"
            value={formValue.matKhau}
            placeholder="input placeholder"
          />
        </Form.Item>
        <Form.Item
          style={{ width: 500 }}
          rules={[{ required: true, message: "This field is required " }]}
          label="Full name"
          name="hoTen"
        >
          <Input
            onChange={handleChange}
            name="hoTen"
            value={formValue.hoTen}
            placeholder="input placeholder"
          />
        </Form.Item>
        <Form.Item
          style={{ width: 500 }}
          rules={[{ required: true, message: "This field is required " }]}
          label="SDT"
          name="soDt"
        >
          <Input
            onChange={handleChange}
            name="soDt"
            value={formValue.soDt}
            placeholder="input placeholder"
          />
        </Form.Item>
        <Form.Item
          style={{ width: 500 }}
          rules={[
            { type: "email", message: "The input is not valid E-mail!" },
            { required: true, message: "Please input your E-mail!" },
          ]}
          label="Email"
          name="email"
        >
          <Input
            onChange={handleChange}
            name="email"
            value={formValue.email}
            placeholder="input placeholder"
          />
        </Form.Item>
        <Form.Item style={{ width: 500 }} label="User Type">
          <Input
            name="maLoaiNguoiDung"
            onChange={handleChange}
            value={formValue.maLoaiNguoiDung}
          />
        </Form.Item>

        <Form.Item>
          <Button
            className="bg-blue-400 text-white"
            type="primary"
            onClick={handleSubmit}
          >
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
