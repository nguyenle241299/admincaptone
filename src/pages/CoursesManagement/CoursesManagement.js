import React, { useEffect, useState } from "react";
import { Button, message, Space, Table, Tag } from "antd";
import { adminCoursesService } from "./../../service/adminCouresService";

import { useDispatch, useSelector } from "react-redux";
import {
  setCoursesList,
  setDeleteCourses,
} from "../../redux_toolkit/coursesSlice";
import FormAddCourses from "./FormAddCourses";
import FormUpdateCourses from "./FormUpdateCourses";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFaceSadCry,
  faFaceSadTear,
  faSearch,
  faSpinner,
} from "@fortawesome/free-solid-svg-icons";

export default function CoursesManagement() {
  const dispatch = useDispatch();
  const [show, setShow] = useState(false);
  const [valuesSearch, setValuesSearch] = useState("");
  const [listCoursesFilter, setListCoursesFilter] = useState([]);
  const [showUpdate, setShowUpdate] = useState(false);
  const listCourses = useSelector((state) => {
    return state.coursesSlice.coursesList;
  });
  useEffect(() => {
    adminCoursesService
      .getCoursesList()
      .then((res) => {
        dispatch(setCoursesList(res.data));
      })
      .catch((err) => {
        console.log(err);
        setTimeout(() => {
          window.location.reload();
        }, 500);
      });
  }, []);

  const handleDeleteCourse = (item) => {
    adminCoursesService
      .deleteCourses(item.maKhoaHoc)
      .then((res) => {
        message.success("Xóa thành công");
        dispatch(setDeleteCourses(item.maKhoaHoc));
      })
      .catch((err) => {
        message.error(err.response.data);
        setTimeout(() => {
          window.location.reload();
        }, 500);
      });
  };
  const handleRenderCoursesItem = () => {
    return listCourses?.map((item, index) => {
      return (
        <tr
          key={index}
          className="bg-white border-b dark:bg-gray-900 dark:border-gray-700"
        >
          <th
            scope="row"
            className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
          >
            {item.maKhoaHoc}
          </th>
          <td className="px-6 py-4">{item.tenKhoaHoc}</td>
          <td className="px-6 py-4">{item.ngayTao}</td>
          <td className="px-6 py-4">
            {item?.nguoiTao?.hoTen ? item.nguoiTao.hoTen : "Ẩn danh"}
          </td>
          <td className="px-6 py-4">
            <button
              onClick={() => {
                handleDeleteCourse(item);
              }}
              className="mr-2 px-3 rounded-sm py-1 bg-red-600 text-white hover:opacity-75 transition-all"
            >
              Xóa
            </button>
            <button
              onClick={() => {
                setShowUpdate(!showUpdate);
              }}
              className="px-3 mt-2 rounded-sm py-1 bg-sky-600 text-white hover:opacity-75 transition-all"
            >
              Sửa
            </button>
            {showUpdate ? (
              <FormUpdateCourses
                showUpdate={setShowUpdate}
                itemCourses={item}
              />
            ) : null}
          </td>
        </tr>
      );
    });
  };
  const handleChangeSearchCourses = (e) => {
    setValuesSearch(e.target.value);
    const filterCourses = listCourses?.filter((item) => {
      return item.tenKhoaHoc.toLowerCase().includes(valuesSearch.toLowerCase());
    });
    return setListCoursesFilter(filterCourses);
  };
  const handleRenderSearchListCoursesItem = () => {
    if (listCoursesFilter.length == 0) {
      return (
        <tr className="text-red-600 text-xl h-10 flex items-center ml-4">
          No results for: {valuesSearch}
          <FontAwesomeIcon
            className="text-indigo-300 ml-3"
            icon={faFaceSadTear}
          />
          <span className="ml-2">...</span>
        </tr>
      );
    } else {
      return listCoursesFilter?.map((item, index) => {
        return (
          <tr
            key={index}
            className="bg-white border-b dark:bg-gray-900 dark:border-gray-700"
          >
            <th
              scope="row"
              className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
            >
              {item.maKhoaHoc}
            </th>
            <td className="px-6 py-4">{item.tenKhoaHoc}</td>
            <td className="px-6 py-4">{item.ngayTao}</td>
            <td className="px-6 py-4">
              {item?.nguoiTao?.hoTen ? item.nguoiTao.hoTen : "Ẩn danh"}
            </td>
            <td className="px-6 py-4">
              <button
                onClick={() => {
                  handleDeleteCourse(item);
                }}
                className="mr-2 px-3 rounded-sm py-1 bg-red-600 text-white hover:opacity-75 transition-all"
              >
                Xóa
              </button>
              <button
                onClick={() => {
                  setShowUpdate(!showUpdate);
                }}
                className="px-3 mt-2 rounded-sm py-1 bg-sky-600 text-white hover:opacity-75 transition-all"
              >
                Sửa
              </button>
              {showUpdate ? (
                <FormUpdateCourses
                  showUpdate={setShowUpdate}
                  itemCourses={item}
                />
              ) : null}
            </td>
          </tr>
        );
      });
    }
  };
  const handleSearchSpinner = (e) => {
    e.preventDefault();
  };
  const handleSearchSearch = (e) => {
    e.preventDefault();
  };
  return (
    <div className="relative overflow-x-auto shadow-md">
      <div className="flex item items-center justify-end">
        <button
          onClick={() => {
            setShow(true);
          }}
          className="px-2 py-1 bg-green-500 text-while rounded-sm "
        >
          Thêm
        </button>
        {show ? <FormAddCourses show={setShow} /> : null}

        <form
          action=" "
          className="flex items-center justify-between ml-5 h-9 bg-white px-2 shadow"
        >
          <input
            type="text"
            className="focus:outline-none"
            onChange={handleChangeSearchCourses}
            placeholder="Search courses"
          />

          {valuesSearch.length > 0 ? (
            <button onClick={handleSearchSpinner}>
              <FontAwesomeIcon
                className="animate-spin text-teal-500"
                icon={faSpinner}
              />
            </button>
          ) : (
            <button onClick={handleSearchSearch}>
              <FontAwesomeIcon
                className="text-teal-500 hover:opacity-50"
                icon={faSearch}
              />
            </button>
          )}
        </form>
      </div>
      <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
        <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
          <tr>
            <th scope="col" className="px-6 py-3">
              Mã Khóa Học
            </th>
            <th scope="col" className="px-6 py-3">
              Tên Khóa Học
            </th>
            <th scope="col" className="px-6 py-3">
              Ngày Tạo
            </th>
            <th scope="col" className="px-6 py-3">
              Người Tạo
            </th>
            <th scope="col" className="px-6 py-3">
              Hành Động
            </th>
          </tr>
        </thead>
        <tbody>
          {valuesSearch.length > 0
            ? handleRenderSearchListCoursesItem()
            : handleRenderCoursesItem()}
        </tbody>
      </table>
    </div>
  );
}
