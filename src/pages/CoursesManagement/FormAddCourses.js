import React from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faXmark } from "@fortawesome/free-solid-svg-icons";
import { adminCoursesService } from "../../service/adminCouresService";
import { message } from "antd";
import { useDispatch } from "react-redux";
import { setAddCourses } from "../../redux_toolkit/coursesSlice";

export default function FormAddCourses({ show }) {
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      maKhoaHoc: "",
      biDanh: "Giáo Vụ",
      tenKhoaHoc: "",
      moTa: "",
      luotXem: 123,
      danhGia: 280,
      hinhAnh: "",
      maNhom: "GP01",
      ngayTao: "",
      maDanhMucKhoaHoc: "",
      taiKhoanNguoiTao: "",
    },
    validationSchema: Yup.object({
      maKhoaHoc: Yup.string().required("Vui lòng nhập trường này !"),
      tenKhoaHoc: Yup.string().required("Vui lòng nhập trường này !"),
      moTa: Yup.string().required("Vui lòng nhập trường này !"),
      hinhAnh: Yup.string().required("Vui lòng nhập trường này !"),
      ngayTao: Yup.string().required("Vui lòng nhập trường này !"),
      maDanhMucKhoaHoc: Yup.string().required("Vui lòng nhập trường này !"),
      taiKhoanNguoiTao: Yup.string().required("Vui lòng nhập trường này !"),
    }),
    onSubmit: (values) => {
      adminCoursesService
        .postCourses(values)
        .then((res) => {
          message.success("Thêm khóa học thành công !");
          dispatch(setAddCourses(values));
          show(false);
        })
        .catch((err) => {
          message.error(err.response.data);
          setTimeout(() => {
            window.location.reload();
          }, 500);
        });
    },
  });
  return (
    <div className="fixed top-0 right-0 left-0 bottom-0 z-200 flex item center justify-center bg-gray-200/[0.5]">
      <div className="flex w-full h-full flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
        <div className=" bg-white w-96 rounded-lg shadow  md:mt-0 sm:max-w-md xl:p-0">
          <form onSubmit={formik.handleSubmit} className="p-4" action="#">
            <div className="text-right">
              <FontAwesomeIcon
                onClick={() => {
                  show(false);
                }}
                className=" text-red-400 cursor-pointer hover:text-teal-600"
                icon={faXmark}
              />
            </div>
            <div class="relative z-0 w-full mb-6 group">
              <input
                type="text"
                name="maKhoaHoc"
                id="maKhoaHoc"
                onChange={formik.handleChange}
                class="block py-2 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-teal-400 peer"
                placeholder=" "
              />
              {formik.errors.maKhoaHoc && (
                <h2 className="text-red-500 text-sm">
                  {formik.errors.maKhoaHoc}
                </h2>
              )}
              <label
                for="maKhoaHoc"
                class="peer-focus:font-medium absolute text-sm text-gray-500 duration-300 transform -translate-y-6 scale-75 top-5 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-teal-500  peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
              >
                Mã Khóa Học
              </label>
            </div>
            <div class="relative z-0 w-full mb-6 group">
              <input
                type="text"
                name="tenKhoaHoc"
                id="tenKhoaHoc"
                onChange={formik.handleChange}
                class="block py-2 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-teal-400 peer"
                placeholder=" "
              />
              {formik.errors.tenKhoaHoc && (
                <h2 className="text-red-500 text-sm">
                  {formik.errors.tenKhoaHoc}
                </h2>
              )}
              <label
                for="tenKhoaHoc"
                class="peer-focus:font-medium absolute text-sm text-gray-500 duration-300 transform -translate-y-6 scale-75 top-5 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-teal-500  peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
              >
                Tên Khóa Học
              </label>
            </div>
            <div class="relative z-0 w-full mb-6 group">
              <input
                type="text"
                name="moTa"
                id="moTa"
                onChange={formik.handleChange}
                class="block py-2 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-teal-400 peer"
                placeholder=" "
              />
              {formik.errors.moTa && (
                <h2 className="text-red-500 text-sm">{formik.errors.moTa}</h2>
              )}
              <label
                for="moTa"
                class="peer-focus:font-medium absolute text-sm text-gray-500 duration-300 transform -translate-y-6 scale-75 top-5 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-teal-500  peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
              >
                Mô Tả
              </label>
            </div>
            <div class="relative z-0 w-full mb-6 group">
              <input
                type="text"
                name="hinhAnh"
                id="hinhAnh"
                onChange={formik.handleChange}
                class="block py-2 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-teal-400 peer"
                placeholder=" "
              />
              {formik.errors.hinhAnh && (
                <h2 className="text-red-500 text-sm">
                  {formik.errors.hinhAnh}
                </h2>
              )}
              <label
                for="hinhAnh"
                class="peer-focus:font-medium absolute text-sm text-gray-500 duration-300 transform -translate-y-6 scale-75 top-5 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-teal-500  peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
              >
                Hình Ảnh
              </label>
            </div>
            <div class="relative z-0 w-full mb-6 group">
              <input
                type="text"
                name="ngayTao"
                id="ngayTao"
                onChange={formik.handleChange}
                class="block py-2 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-teal-400 peer"
                placeholder=" "
              />
              {formik.errors.ngayTao && (
                <h2 className="text-red-500 text-sm">
                  {formik.errors.ngayTao}
                </h2>
              )}
              <label
                for="ngayTao"
                class="peer-focus:font-medium absolute text-sm text-gray-500 duration-300 transform -translate-y-6 scale-75 top-5 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-teal-500  peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
              >
                Ngày Tạo
              </label>
            </div>
            <div class="relative z-0 w-full mb-6 group">
              <input
                type="text"
                name="maDanhMucKhoaHoc"
                id="maDanhMucKhoaHoc"
                onChange={formik.handleChange}
                class="block py-2 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-teal-400 peer"
                placeholder=" "
              />
              {formik.errors.maDanhMucKhoaHoc && (
                <h2 className="text-red-500 text-sm">
                  {formik.errors.maDanhMucKhoaHoc}
                </h2>
              )}
              <label
                for="maDanhMucKhoaHoc"
                class="peer-focus:font-medium absolute text-sm text-gray-500 duration-300 transform -translate-y-6 scale-75 top-5 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-teal-500  peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
              >
                Mã Danh Mục Khóa Học
              </label>
            </div>
            <div class="relative z-0 w-full mb-6 group">
              <input
                type="text"
                name="taiKhoanNguoiTao"
                id="taiKhoanNguoiTao"
                onChange={formik.handleChange}
                class="block py-2 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-teal-400 peer"
                placeholder=" "
              />
              {formik.errors.taiKhoanNguoiTao && (
                <h2 className="text-red-500 text-sm">
                  {formik.errors.taiKhoanNguoiTao}
                </h2>
              )}
              <label
                for="taiKhoanNguoiTao"
                class="peer-focus:font-medium absolute text-sm text-gray-500 duration-300 transform -translate-y-6 scale-75 top-5 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-teal-500  peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
              >
                Tài Khoản Người Tạo
              </label>
            </div>
            <button
              type="submit"
              className="w-full text-white bg-teal-400 py-2 rounded hover:opacity-70"
            >
              Thêm
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}
