import { message } from "antd";
import { useFormik } from "formik";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import * as Yup from "yup";
import { setAdminInfo } from "../../redux_toolkit/adminSlice";
import { userLocalStorage } from "../../service/adminLocalService";
import { adminService } from "../../service/adminService";
export default function LoginPage() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const userAdmin = useSelector((state) => {
    return state.adminSlice.admin;
  });
  const formik = useFormik({
    initialValues: {
      taiKhoan: "",
      matKhau: "",
    },
    validationSchema: Yup.object({
      taiKhoan: Yup.string().required("Please enter this field"),
      matKhau: Yup.string().required("Please enter this field"),
    }),

    onSubmit: (values) => {
      adminService
        .postLogin(values)
        .then((res) => {
          dispatch(setAdminInfo(res.data));
          if (userAdmin?.maLoaiNguoiDung == "GV") {
            message.success("Đăng nhập thành công");
            setTimeout(() => {
              navigate("/home");
            }, 500);
            userLocalStorage.set(res.data);
          } else {
            message.error(
              "Đăng nhập không thành công , vì tài khoản không đủ phân quyền để truy cập"
            );
          }
        })
        .catch((err) => {
          message.error(err.response.data);
          setTimeout(() => {
            window.location.reload();
          }, 500);
        });
    },
  });
  return (
    <section className="bg-gray-50 ">
      <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
        <a href="#" className="flex items-center mb-6 text-2xl font-semibold ">
          <img
            className="w-8 h-8 mr-2"
            src="https://flowbite.s3.amazonaws.com/blocks/marketing-ui/logo.svg"
            alt="logo"
          />
          LOGIN
        </a>
        <div className="w-full bg-white rounded-lg shadow  md:mt-0 sm:max-w-md xl:p-0 ">
          <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
            <h1 className="text-xl font-bold leading-tight tracking-tight md:text-2xl">
              Sign in to your account
            </h1>
            <form
              onSubmit={formik.handleSubmit}
              className="space-y-4 md:space-y-6"
              action="#"
            >
              <div>
                <label
                  htmlFor="email"
                  className="block mb-2 text-sm font-medium text-gray-900 "
                >
                  User Name
                </label>
                <input
                  type="text"
                  name="taiKhoan"
                  id="taiKhoan"
                  onChange={formik.handleChange}
                  className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg block w-full p-2.5 focus:border-transparent focus:border focus:outline-emerald-400 hover:border-emerald-500"
                  placeholder="name123"
                />
                {formik.errors.taiKhoan && (
                  <h2 className="text-red-500 text-sm">
                    {formik.errors.taiKhoan}
                  </h2>
                )}
              </div>
              <div>
                <label
                  htmlFor="password"
                  className="block mb-2 text-sm font-medium text-gray-900 "
                >
                  Password
                </label>
                <input
                  type="password"
                  name="matKhau"
                  id="matKhau"
                  placeholder="••••••••"
                  onChange={formik.handleChange}
                  className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg  block w-full p-2.5 focus:border-transparent focus:border focus:outline-emerald-400 hover:border-emerald-500"
                />
                {formik.errors.matKhau && (
                  <h2 className="text-red-500 text-sm">
                    {formik.errors.matKhau}
                  </h2>
                )}
              </div>
              <div className="flex items-center justify-between">
                <div className="flex items-start">
                  <div className="flex items-center h-5">
                    <input
                      id="remember"
                      aria-describedby="remember"
                      type="checkbox"
                      className="w-4 h-4 border border-gray-300 rounded bg-gray-50 focus:ring-3 focus:ring-yellow-300 cursor-pointer"
                    />
                  </div>
                  <div className="ml-3 text-sm">
                    <label htmlFor="remember" className="text-teal-400">
                      Remember me
                    </label>
                  </div>
                </div>
                <a
                  href="#"
                  className="text-sm font-medium text-primary-600 hover:underline dark:text-primary-500"
                >
                  Forgot password?
                </a>
              </div>
              <button
                type="submit"
                className="w-full text-white bg-teal-500 hover:opacity-70 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800"
              >
                Sign in
              </button>
            </form>
          </div>
        </div>
      </div>
    </section>
  );
}
