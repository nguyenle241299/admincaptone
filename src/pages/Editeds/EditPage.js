import React, { useEffect } from "react";
import { Button, Form, Input, message, Select } from "antd";
import { useDispatch, useSelector } from "react-redux";

import { Option } from "antd/es/mentions";
import { useNavigate, useParams } from "react-router-dom";
import { useFormik } from "formik";
import { adminUserService } from "./../../service/adminUserService";
import { setUserUpdate } from "../../redux_toolkit/AdminUserSlice";
// import UserInfo from './../../components/user-info/UserInfo';

export default function AddNewusers() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const formik = useFormik({
    initialValues: {
      taiKhoan: "",
      matKhau: "",
      hoTen: "",
      soDT:"",
      maLoaiNguoiDung: "",
      maNhom: "GP05",
      email: "",
    },
    onSubmit: (values) => {
      console.log("values: ", values);
      adminUserService
        .updateUser(values)
        .then((res) => {
          message.success("Successful");
          dispatch(setUserUpdate(values));
        })
        .catch((err) => {
          message.error("Failed");
        });
    },
  });
  return (
    <div>
      <Form onSubmitCapture={formik.onSubmit}>
        <Form.Item
          style={{ width: 500 }}
          rules={[{ required: true, message: "This field is required " }]}
          label="username"
          name="taiKhoan"
        >
          <Input
            value={formik.taiKhoan}
            name="taiKhoan"
            onChange={formik.handleChange}
            placeholder="input placeholder"
          />
        </Form.Item>
        <Form.Item
          style={{ width: 500 }}
          rules={[{ required: true, message: "This field is required" }]}
          label="password"
          name="matKhau"
        >
          <Input
            value={formik.matKhau}
            name="matKhau"
            onChange={formik.handleChange}
            placeholder="input placeholder"
          />
        </Form.Item>
        <Form.Item
          style={{ width: 500 }}
          rules={[{ required: true, message: "This field is required " }]}
          label="Full name"
          name="hoTen"
        >
          <Input
            value={formik.hoTen}
            name="hoTen"
            onChange={formik.handleChange}
            placeholder="input placeholder"
          />
        </Form.Item>
        <Form.Item
          style={{ width: 500 }}
          rules={[{ required: true, message: "This field is required " }]}
          label="SDT"
          name="soDt"
        >
          <Input
            value={formik.soDt}
            name="soDt"
            onChange={formik.handleChange}
            placeholder="input placeholder"
          />
        </Form.Item>
        <Form.Item
          style={{ width: 500 }}
          rules={[
            { type: "email", message: "The input is not valid E-mail!" },
            { required: true, message: "Please input your E-mail!" },
          ]}
          label="Email"
          name="email"
        >
          <Input
            value={formik.email}
            name="email"
            onChange={formik.handleChange}
            placeholder="input placeholder"
          />
        </Form.Item>
        <Form.Item style={{ width: 500 }} label="User Type">
          <Select style={{ width: 120 }}>
            <Option value="0">HV</Option>
            <Option value="1">GV</Option>
          </Select>
        </Form.Item>

        <Form.Item>
          <Button
            onClick={formik.onSubmit}
            className="bg-blue-400 text-white"
            type="submit"
          >
            Update
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
