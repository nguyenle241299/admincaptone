import React from "react";
import DashBoard from "./DashBoard/DashBoard";

export default function HomePage() {
  return (
    <div>
      <DashBoard />
    </div>
  );
}
